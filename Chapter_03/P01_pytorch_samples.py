import torch
a = torch.FloatTensor([2, 3,3,4])
#ca = a.cuda()
torch.cuda.is_available()
a.requires_grad

print(a)

import torch.nn as nn
l = nn.Linear(2,5)
v = torch.FloatTensor([1,2])
l(v)

s = nn.Sequential(
    nn.Linear(2,5),
    nn.ReLU(),
    nn.Linear(5,20),
    nn.ReLU(),
    nn.Linear(20, 10),
    nn.Dropout(p=0.3),
    nn.Softmax(dim=1)
)
s(torch.FloatTensor([[1,2]]))


for batch_samples, batch_labels in iterate_batches(data, batch_size=32):
    batch_samples_t = torch.tensor(batch_samples)
    batch_labels_t = torch.tensor(batch_labels)
    out_t = net(batch_samples_t)
    loss_t = loss_function(out_t, batch_labels_t)
    loss_t.backward()
    optimizer.step()
    optimizer.zero_grad()
