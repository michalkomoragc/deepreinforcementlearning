import gym


if __name__ == "__main__":

    # starting an environment
    e = gym.make('CartPole-v0')
    e = gym.wrappers.Monitor(e, "recording", force=True)

    total_reward = 0.0
    total_steps = 0

    # reset it before use
    obs = e.reset()

    # Initial observations, action space and observation space
    print("First observations: {}".format(obs))
    print("Action space: {}".format(e.action_space))
    print("Observation space: {}".format(e.observation_space))

    # take a step left
    step = e.step(0)

    # print results afer a step
    print("Next observation: {}".format(step[0]))
    print("Reward: {}".format(step[1]))
    print("Is over?: {}".format(step[2]))

    # environment's samples
    e.action_space.sample()
    e.observation_space.sample()

    while True:
        action = e.action_space.sample()
        obs, reward, done, _ = e.step(action)
        total_reward += reward
        total_steps += 1
        if done:
            break

    print("Episode done in {} steps, total reward {}".
            format(total_steps, total_reward))
