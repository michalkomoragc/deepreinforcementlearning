import gym
import random


class RandomActionWrapper(gym.ActionWrapper):

    def __init__(self, env, epsilon=0.1):
        # depreciated
        # super(RandomActionWrapper, self).__init__(env)
        super().__init__(env)
        self.epsilon = epsilon

    def action(self, action):
        """
        This is a method that we need to override from
        a parent's class to tweak the agent's
        actions. Every time we roll the die and with the probability
        of epsilon, we samplea random action from the action space
        and return it instead of the action the agent
        has sent to us.
        """
        if random.random() < self.epsilon:
            print("Random")
            return self.env.action_space.sample()
        return action


if __name__ == "__main__":
    env = RandomActionWrapper(gym.make("CartPole-v0"))
    

    obs = env.reset()
    total_reward = 0.0

    while True:
        obs, reward, done, _ = env.step(0)
        total_reward += reward
        if done:
            break

    print("Total reward {}".
                format(total_reward))
