
import torch.nn as nn

"""
We define constants at the top of the file and they include the count of neurons in the
hidden layer, the count of episodes we play on every iteration (16), and the percentile
of episodes' total rewards that we use for elite episode filtering. We'll take the 70th
percentile, which means that we'll leave the top 30% of episodes sorted by reward:
"""

HIDDEN_SIZE = 128
BATCH_SIZE = 16
PERCENTILE = 70

class Net(nn.Module):
    def __init__(self, obs_size, hidden_size, n_actions):
        super().__init__()
        self.net = nn.Sequential(
            nn.Linear(obs_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, n_actions)
            )
